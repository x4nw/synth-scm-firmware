
#include <zephyr/kernel.h>
#include <zephyr/drivers/spi.h>
#include "backplane.h"

K_MSGQ_DEFINE(backplane_output_msgq, sizeof(struct backplane_output_array), 1, 1);

static const struct device* midi_dev = DEVICE_DT_GET(DT_ALIAS(midi_uart));
void process_backplane_output(
    const struct spi_dt_spec* const backplane_dev_dt,
    const struct backplane_output_array* const to_write
) {
    const struct spi_buf out = {
        .buf = (uint8_t*)to_write->buf,
        .len = to_write->len,
    };
    const struct spi_buf_set tx_bufs = {
        .buffers = &out,
        .count = 1,
    };
    const int ret = spi_write_dt(backplane_dev_dt, &tx_bufs);
    if (ret != 0) {
        printk("Backplane write failure %d\n", ret);
        return;
    }

    if (to_write->write_dcv) {
        // uart_poll_out
        // do suitable delay and dcv uart write
    }
}

void backplane_output_thread(void* unused1, void* unused2, void* unused3) {
    const struct spi_dt_spec backplane_dev_dt = SPI_DT_SPEC_GET(
        DT_NODELABEL(backplane),
        SPI_WORD_SET(8), 0
    );
    if (!spi_is_ready_dt(&backplane_dev_dt)) {
        printk("Backplane device not ready!\n");
        return;
    }
    while(1) {
        struct backplane_output_array to_write = {0};
        k_msgq_get(&backplane_output_msgq, &to_write, K_FOREVER);
        process_backplane_output(&backplane_dev_dt, &to_write);
    }
}

K_THREAD_DEFINE(                  \
    backplane_output,         \
    1024,                     \
    backplane_output_thread,  \
    NULL, NULL, NULL,         \
    -2, 0, 200                \
);
