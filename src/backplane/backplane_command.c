
#include <zephyr/kernel.h>
#include <zephyr/drivers/spi.h>

#include "backplane.h"

K_MSGQ_DEFINE(backplane_command_msgq, sizeof(struct backplane_command_array), 8, 1);
extern struct k_msgq backplane_output_msgq;

void send_backplane_command(struct backplane_command_array* in) {
    k_msgq_put(&backplane_command_msgq, in, K_NO_WAIT);
}

void construct_short(
    const uint32_t voices[static CONFIG_MAX_VOICES],
    uint8_t* out_buf,
    const size_t len
) {
    const uint8_t n_voices = backplane_get_n_voices();
    size_t out_idx = 0;
    bool high_nibble = true;

    if ((n_voices%2) != 0) {
        out_buf[0] = 0x80;
        high_nibble = false;
    }
    for (size_t i = 0; i < n_voices; i++) {
        const uint8_t inserter = (voices[n_voices-i-1] & 0xF) | 0x8;
        if (out_idx >= len) {
            printk("warning: construct_short buffer overflow\n");
            break;
        }
        if (high_nibble) {
            out_buf[out_idx] = (inserter) << 4;
            high_nibble = false;
        } else {
            out_buf[out_idx] |= inserter;
            high_nibble = true;
            out_idx++;
        }
    }

    if (!high_nibble) {
        printk("warning: construct_short ended mid nibble\n");
    }
}

#include <zephyr/drivers/spi.h>
void construct_long(
    const uint32_t voices[static CONFIG_MAX_VOICES],
    uint8_t* out_buf,
    const size_t len
) {
    // TODO: implement
}

static uint32_t voice_cache[CONFIG_MAX_VOICES];

void process_backplane_command(
    const struct backplane_command_array* command_in
) {
    const size_t n_voices = backplane_get_n_voices();
    for (size_t i = 0; i < n_voices; i++) {
        const struct backplane_command* c = &command_in->commands[i];
        if (c->mask != 0) {
            const uint32_t set_writer = c->bits | c->mask;
            const uint32_t unset_writer = (~c->bits) | c->mask;
            voice_cache[i] |= set_writer;
            voice_cache[i] &= ~unset_writer;
        }
    }

    const size_t out_len = command_in->extended ?
        BYTES_PER_VOICE*n_voices :
        (n_voices / 2 + (n_voices % 2));
    struct backplane_output_array out = {
        .buf = {0},
        .len = out_len,
        .dcv = command_in->dcv,
        .write_dcv = command_in->write_dcv,
    };
    if (command_in->extended) {
        construct_long(voice_cache, out.buf, sizeof(out.buf));
    } else {
        construct_short(voice_cache, out.buf, sizeof(out.buf));
    }

    k_msgq_put(&backplane_output_msgq, &out, K_FOREVER);
}

void backplane_processing_thread(void* unused1, void* unused2, void* unused3) {
    while(1) {
        struct backplane_command_array command_in = {0};
        k_msgq_get(&backplane_command_msgq, &command_in, K_FOREVER);
        process_backplane_command(&command_in);
    }
}

K_THREAD_DEFINE(                      \
    backplane_processing,         \
    1024,                         \
    backplane_processing_thread,  \
    NULL, NULL, NULL,             \
    -1, 0, 200                    \
);
