
#include <stdint.h>

uint8_t n_voices = 1;

void backplane_set_n_voices(uint8_t in) {
    n_voices = in;
}

uint8_t backplane_get_n_voices() {
    return n_voices;
}
