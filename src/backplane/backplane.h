
#ifndef _BACKPLANE_H__
#define _BACKPLANE_H__

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define GATE_SET(value)   (value<<0)
#define GATE_SET_MASK     (1<<0)
#define GATE_CLEAR(value) (value<<1)
#define GATE_CLEAR_MASK   (1<<1)
#define EN_DCV(value)     (value<<2)
#define EN_DCV_MASK       (1<<2)

#define BYTES_PER_VOICE (3)

#define COMMAND_GATE_SET(command, value) do {   \
    command.mask |= GATE_SET_MASK;              \
    command.bits |= GATE_SET(value);            \
} while (0);

#define COMMAND_GATE_CLEAR(command, value) do {   \
    command.mask |= GATE_CLEAR_MASK;              \
    command.bits |= GATE_CLEAR(value);            \
} while (0);

#define COMMAND_EN_DCV(command, value) do {   \
    command.mask |= EN_DCV_MASK;              \
    command.bits |= EN_DCV(value);            \
} while (0);

struct backplane_command {
    uint32_t mask;
    uint32_t bits;
};

struct backplane_command_array {
    struct backplane_command commands[CONFIG_MAX_VOICES];
    uint32_t dcv;
    bool write_dcv;
    bool extended;
};

struct backplane_output_array {
    uint8_t buf[CONFIG_MAX_VOICES*BYTES_PER_VOICE];
    size_t len;
    uint32_t dcv;
    bool write_dcv;
};

void send_backplane_command(struct backplane_command_array* in);

void backplane_set_n_voices(uint8_t n);
uint8_t backplane_get_n_voices(void);

void do_pitch_bend(uint8_t midi_channel, uint16_t bend_value);

#endif // _BACKPLANE_H__
