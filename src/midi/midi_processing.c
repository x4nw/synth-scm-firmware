
#include <zephyr/kernel.h>

#include "midi.h"
#include "backplane.h"

K_MSGQ_DEFINE(midi_processing_msgq, sizeof(struct backplane_output_array), 8, 1);

uint8_t pg_assignment[CONFIG_MAX_VOICES] = {0};

void send_midi_command(struct midi_command* command) {
    k_msgq_put(&midi_processing_msgq, command, K_FOREVER);
}

static void _process_midi_pitch_bend(
    struct midi_configuration* config,
    uint8_t channel,
    uint16_t bend_value
) {
/*
Algorithm description:
  - Command all voices EN_DCV(0)
  - Overlay command all voices of all voice gangs in pg/channel EN_DCV(1)
  - Write command + dcv if any voice was set EN_DCV(1)
*/
    struct backplane_command_array writer = {
        .write_dcv = false,
        .extended = false,
        .commands = {
            [0 ... CONFIG_MAX_VOICES-1] = {
                .mask = EN_DCV_MASK,
                .bits = EN_DCV(0),
            }
        }
    };
    struct polyphonic_group* pg = &config->polyphonic_groups[channel];
    if (!pg->present) {
        // printk("Warning: tried to pitch bend nonexistent pg");
        return;
    }
    const uint8_t n_voices = backplane_get_n_voices();
    for (size_t vg_idx = 0; vg_idx < n_voices; vg_idx++) {
        struct voice_gang* vg = &pg->voice_gangs[vg_idx];
        if (!vg->present) {
            break;
        }
        for (size_t i=0; i < n_voices; i++) {
            if (vg->voice_membership & (1<<i)) {
                writer.write_dcv = true;
                writer.dcv = bend_value;
                COMMAND_EN_DCV(writer.commands[i], 1);
            }
        }
    }
    if (writer.write_dcv) {
        send_backplane_command(&writer);
    }
}

static void process_midi_pitch_bend(uint8_t channel, uint16_t bend_value) {
    struct midi_configuration* config = hold_midi_config();
    _process_midi_pitch_bend(config, channel, bend_value);
    release_midi_config();
}

static void _process_midi_note_off(
    struct midi_configuration* config,
    uint8_t channel,
    uint8_t num
) {
/*
Algorithm description:
  - Look up voice gang playing note on channel*num
  - If found, command GATE_CLEAR of all voices in gang
  - Write command if any voices were affected
*/
    struct voice_gang* vg = config->notes[channel][num];
    if (!vg || !vg->in_use) {
        return;
    }
    struct backplane_command_array writer = {0};
    for (size_t i = 0; i < backplane_get_n_voices(); i++) {
        if (vg->voice_membership & (1<<i)) {
            COMMAND_GATE_CLEAR(writer.commands[i], 1);
        }
    }
    vg->in_use = false;
    config->notes[channel][num] = NULL;
    send_backplane_command(&writer);
}

static void process_midi_note_off(uint8_t channel, uint8_t num) {
    struct midi_configuration* config = hold_midi_config();
    _process_midi_note_off(config, channel, num);
    release_midi_config();
}

static void _process_midi_note_on(
    struct midi_configuration* config,
    uint8_t channel,
    uint8_t num,
    uint8_t velocity
) {
/*
Algorithm description (if velocity > 0):
  - Find unused voice gang in pg/channel, return if none found
  - Command all voices EN_DCV(0)
  - Overlay command GATE_SET, EN_DCV(1) + dcv of all voices in gang
*/
    if (velocity == 0) {
        process_midi_note_off(channel, num);
        return;
    }
    struct polyphonic_group* pg = &config->polyphonic_groups[channel];
    struct voice_gang* vg = NULL;
    for (size_t i = 0; i < backplane_get_n_voices(); i++) {
        vg = &pg->voice_gangs[i];
        if (!vg->present) {
            return;
        } else if (vg->in_use) {
            continue;
        } else if (vg->voice_membership) { // Only attempt to use vg if it has assigned voices
            break;
        }
    }

    if (!vg) {
        return;
    }

    // Suitable vg found
    struct backplane_command_array writer = {
        .write_dcv = true,
        .dcv = 0, // TODO: implement
        .commands = {
            [0 ... CONFIG_MAX_VOICES-1] = {
                .mask = EN_DCV_MASK,
                .bits = EN_DCV(0),
            }
        }
    };

    for (size_t i = 0; i < backplane_get_n_voices(); i++) {
        if (vg->voice_membership & (1<<i)) {
            COMMAND_GATE_SET(writer.commands[i], 1);
            COMMAND_EN_DCV(writer.commands[i], 1);
        }
    }

    vg->in_use = true;
    config->notes[channel][num] = vg;
    send_backplane_command(&writer);
}

static void process_midi_note_on(uint8_t channel, uint8_t num, uint8_t velocity) {
    struct midi_configuration* config = hold_midi_config();
    _process_midi_note_on(config, channel, num, velocity);
    release_midi_config();
}

static void process_midi_command(const struct midi_command* command) {
    switch (command->message) {
    case 0b1000: {
        process_midi_note_off(command->channel, command->db1);
        break;
    }
    case 0b1001: {
        process_midi_note_on(command->channel, command->db1, command->db2);
        break;
    }
    case 0b1010: {
        // TODO: implement polyphonic pressure
        break;
    }
    case 0b1101: {
        // TODO: implement channel pressure
        break;
    }
    case 0b1110: {
        const uint16_t bend_value = (command->db2<<8) | command->db1;
        process_midi_pitch_bend(command->channel, bend_value);
        break;
    }
    };
}

static void midi_processing_thread(void* unused1, void* unused2, void* unused3) {
    while(1) {
        struct midi_command command_in = {0};
        k_msgq_get(&midi_processing_msgq, &command_in, K_FOREVER);
        process_midi_command(&command_in);
    }
}

K_THREAD_DEFINE(                  \
    midi_processing,              \
    1024,                         \
    midi_processing_thread,       \
    NULL, NULL, NULL,             \
    -1, 0, 200                    \
);
