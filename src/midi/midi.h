
#ifndef _MIDI_UART_H__
#define _MIDI_UART_H__

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

struct midi_command {
    uint8_t message;
    uint8_t channel;
    uint8_t db1;
    uint8_t db2;
};

struct voice_gang {
    bool present;
    bool in_use;
    size_t voice_membership;
};

struct polyphonic_group {
    bool present;
    struct voice_gang voice_gangs[CONFIG_MAX_VOICES];
};

struct midi_configuration {
    struct polyphonic_group polyphonic_groups[CONFIG_MAX_VOICES];
    struct voice_gang* notes[128][CONFIG_MAX_VOICES];
};

struct midi_configuration* hold_midi_config(void);
void release_midi_config(void);

void set_midi_configuration(struct midi_configuration* config_in);

void send_midi_command(struct midi_command* command);

#endif // _MIDI_UART_H__
