
#include <zephyr/kernel.h>
#include <zephyr/drivers/uart.h>

#include "midi.h"

struct midi_configuration conf = {0};

K_MUTEX_DEFINE(configuration_lock);
K_MSGQ_DEFINE(midi_msgq, 1, 64, 1);

int midi_reconfigure(struct midi_configuration* conf_in) {
    k_mutex_lock(&configuration_lock, K_FOREVER);
    conf = *conf_in;
    k_mutex_unlock(&configuration_lock);
    return 0;
}

void midi_isr(const struct device *dev, void *user_data)
{
    uint8_t c;
    if (!uart_irq_update(dev)) {
        return;
    }
    if (!uart_irq_rx_ready(dev)) {
        return;
    }
    while (uart_fifo_read(dev, &c, 1) == 1) {
        k_msgq_put(&midi_msgq, &c, K_NO_WAIT);
    }
}


const struct device* midi_dev = DEVICE_DT_GET(DT_ALIAS(midi_uart));

bool message_complete(uint8_t* buf, size_t len) {
    if (len >= 3) {
        return true;
    }
    uint8_t checker = buf[0] >> 4;
    if (len == 2 && checker == 0xd) {
        return true;
    }
    return false;
}

void midi_uart_thread(void* unused1, void* unused2, void* unused3) {
    if (!device_is_ready(midi_dev)) {
        printk("UART device not found!");
        return;
    }
    uart_irq_callback_user_data_set(midi_dev, midi_isr, NULL);
    uart_irq_rx_enable(midi_dev);
    size_t pos = 0;
    uint8_t db[2];
    uint32_t first_byte_received_at = 0;
    uint8_t midi_message = 0;
    uint8_t midi_channel = 0;
    while(1) {
        uint8_t c;
        k_msgq_get(&midi_msgq, &c, K_FOREVER);
        uint32_t now = k_uptime_get_32();
        if (now - first_byte_received_at > 5) {
            pos = 0;
            first_byte_received_at = now;
        }
        if (c | 0x80) {
            pos = 0;
            midi_message = c >> 4;
            midi_channel = c & 0xF;
            continue;
        }
        db[pos] = c;
        pos++;
        if (pos > 1 || (midi_message == 0b1101)) {
            pos = 0;
            struct midi_command command = {
                .message = midi_message,
                .channel = midi_channel,
                .db1 = db[0],
                .db2 = db[1],
            };
            send_midi_command(&command);
        }
    }
}

K_THREAD_DEFINE(       \
    midi_uart,         \
    2048,              \
    midi_uart_thread,  \
    NULL, NULL, NULL,  \
    -2, 0, 200         \
);
