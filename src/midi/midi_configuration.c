

#include <zephyr/kernel.h>

#include "midi.h"
#include "backplane.h"

K_MUTEX_DEFINE(midi_configuration_lock);

struct midi_configuration _midi_configuration = {
    .polyphonic_groups = {
        [0] = {
            .present = true,
            .voice_gangs = {
                [0] = {
                    .present = true,
                    .voice_membership = 0b00000001,
                },
            }
        },
    },
};

struct midi_configuration* hold_midi_config() {
    k_mutex_lock(&midi_configuration_lock, K_FOREVER);
    return &_midi_configuration;
}

void release_midi_config() {
    k_mutex_unlock(&midi_configuration_lock);
}

void set_midi_configuration(struct midi_configuration* config_in) {
    k_mutex_lock(&midi_configuration_lock, K_FOREVER);
    struct backplane_command_array writer = {
        .write_dcv = true,
        .dcv = 0, // TODO: send bend=0
        .commands = {
            [0 ... CONFIG_MAX_VOICES-1] = {
                .mask = EN_DCV_MASK | GATE_CLEAR_MASK,
                .bits = EN_DCV(1) | GATE_CLEAR(1),
            },
        },
    };
    send_backplane_command(&writer);
    _midi_configuration = *config_in;
    k_mutex_unlock(&midi_configuration_lock);
}
