#include "py/builtin.h"
#include "py/runtime.h"

#include "midi/midi.h"
#include "backplane/backplane.h"

// reconfigure()
static mp_obj_t py_midi_reconfigure(void) {
    // struct midi_configuration writer = {
    //     .asdf = 0xFF,
    // };
    // midi_reconfigure(&writer);
    // backplane_set_n_voices(asdf);
    return MP_OBJ_NEW_SMALL_INT(0xFF);
}
MP_DEFINE_CONST_FUN_OBJ_0(midi_reconfigure_obj, py_midi_reconfigure);

static const mp_rom_map_elem_t mp_module_midi_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_midi) },
    { MP_ROM_QSTR(MP_QSTR_reconfigure), MP_ROM_PTR(&midi_reconfigure_obj) },
};
static MP_DEFINE_CONST_DICT(mp_module_midi_globals, mp_module_midi_globals_table);

const mp_obj_module_t mp_module_midi = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t *)&mp_module_midi_globals,
};

MP_REGISTER_MODULE(MP_QSTR_midi, mp_module_midi);
