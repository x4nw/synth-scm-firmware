
#include <zephyr/console/console.h>

// micropython zephyr port main calls itself "real_main"
int real_main(void);

int main() {
    console_init();
    real_main();
}
