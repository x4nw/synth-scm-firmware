
## How to clone this repository and its dependencies

1. Install west if not installed `pip3 install west`
2. `west init -m <this repository's url> synth`
3. `cd synth && west update`

## How to build this repository

1. Install "zephyr sdk" if not installed
2. cd to nested synth folder synth/synth
3. `west build -b synth_control`

